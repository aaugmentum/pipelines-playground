## [1.1.2](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.1...v1.1.2) (2023-06-13)


### Bug Fixes

* update index.js ([47b3cef](https://gitlab.com/aaugmentum/pipelines-playground/commit/47b3cef18e12868eef0ac9a4bc8a10948493a6a4))

## [1.1.1](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.0...v1.1.1) (2023-06-13)


### Bug Fixes

* some-fix ([92d399b](https://gitlab.com/aaugmentum/pipelines-playground/commit/92d399b5c07967fd9d7aaa1b447b247f4f404d10))

# [1.1.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.0.0...v1.1.0) (2023-06-13)


### Features

* some-feature ([9164a88](https://gitlab.com/aaugmentum/pipelines-playground/commit/9164a888356b01f7f88d09404e2d1745b54f6560))
## [1.0.1](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.0.0...v1.0.1) (2023-06-13)


### Bug Fixes

* some-fix ([92d399b](https://gitlab.com/aaugmentum/pipelines-playground/commit/92d399b5c07967fd9d7aaa1b447b247f4f404d10))

# 1.0.0 (2023-06-13)


### Bug Fixes

* fix lint pipeline ([f4cbbdd](https://gitlab.com/aaugmentum/pipelines-playground/commit/f4cbbddcae9f7756a13c9032952032c0cf7c4d2d))
* fix lint pipeline ([9a30064](https://gitlab.com/aaugmentum/pipelines-playground/commit/9a300648fe5c57dd8f8f02861aa2ede14778e65b))
* haha ([8884ec3](https://gitlab.com/aaugmentum/pipelines-playground/commit/8884ec3d253ceb210345e0308e798af981dda07a))
* new feature ([0eabc3b](https://gitlab.com/aaugmentum/pipelines-playground/commit/0eabc3b0330caab99bf9f98cd3562264ca1ca06c))
* update .gitlab-ci.yml ([78b645a](https://gitlab.com/aaugmentum/pipelines-playground/commit/78b645a057020e97527794f13c5935b976b00d4f))
* update .gitlab-ci.yml file ([d71a056](https://gitlab.com/aaugmentum/pipelines-playground/commit/d71a056ce4bdde02818566446d53fe073bc3459d))
* update ver ([89bc931](https://gitlab.com/aaugmentum/pipelines-playground/commit/89bc931fc8fdfab3ecdd6f055635cfb8247185b0))
* update ver ([7a4c5bc](https://gitlab.com/aaugmentum/pipelines-playground/commit/7a4c5bc4e41d7ff5d7f27013b63a6c021b9bfff7))
* version artifact ([5ef9aa7](https://gitlab.com/aaugmentum/pipelines-playground/commit/5ef9aa780dc974779708d1eb92644dc73c976c81))


### Continuous Integration

* Update gitlab-ci.yml ([a76d118](https://gitlab.com/aaugmentum/pipelines-playground/commit/a76d1189799d6e81c8d7ea993e5c2c5743aac6cc))


### Features

* add index.js ([b9fc323](https://gitlab.com/aaugmentum/pipelines-playground/commit/b9fc323a020315acbe0a94fbb0194bf6c5045206))
* add log ([0d9c100](https://gitlab.com/aaugmentum/pipelines-playground/commit/0d9c10007e97671d8c05aebce8256c65e419d23e))
* add prerelease branch ([1a603c0](https://gitlab.com/aaugmentum/pipelines-playground/commit/1a603c0028f08475f382f1bbc9da62e47821a72e))
* hahaha ([abc37d4](https://gitlab.com/aaugmentum/pipelines-playground/commit/abc37d4b4562afc0c92a02fa4f17f77adb2c6865))
* new feature ([99ecbe7](https://gitlab.com/aaugmentum/pipelines-playground/commit/99ecbe74d7b3eca6b90ffd5e71c3e959b45d7776))
* update .gitlab-ci.yml file ([6ce0e2e](https://gitlab.com/aaugmentum/pipelines-playground/commit/6ce0e2e7f15996dc1392eff8e2e3747e27581e24))
* update .gitlab-ci.yml file ([9b4d101](https://gitlab.com/aaugmentum/pipelines-playground/commit/9b4d1016dd01bba4df058d842a65c9ea62d1d4b3))
* update .releaserc.json ([8b8b1cc](https://gitlab.com/aaugmentum/pipelines-playground/commit/8b8b1ccefcb80bcf556d92fbf12d9994b9b5d849))
* update .releaserc.json ([e6fd523](https://gitlab.com/aaugmentum/pipelines-playground/commit/e6fd523d2ed1123c0781a7e561efbfa89cf77aae))
* version 1.0.0 ([7253b51](https://gitlab.com/aaugmentum/pipelines-playground/commit/7253b515d1ac0a8cd503e7c96d79c39fe94212ec))
* version artifact ([284e0d3](https://gitlab.com/aaugmentum/pipelines-playground/commit/284e0d31335a108bf70af4cf9363549c264f394c))


### BREAKING CHANGES

*

## [1.8.1](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.8.0...v1.8.1) (2023-06-12)


### Bug Fixes

* new feature ([0eabc3b](https://gitlab.com/aaugmentum/pipelines-playground/commit/0eabc3b0330caab99bf9f98cd3562264ca1ca06c))

# [1.8.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.7.0...v1.8.0) (2023-06-12)


### Bug Fixes

* haha ([8884ec3](https://gitlab.com/aaugmentum/pipelines-playground/commit/8884ec3d253ceb210345e0308e798af981dda07a))
* update .gitlab-ci.yml file ([d71a056](https://gitlab.com/aaugmentum/pipelines-playground/commit/d71a056ce4bdde02818566446d53fe073bc3459d))
* version artifact ([5ef9aa7](https://gitlab.com/aaugmentum/pipelines-playground/commit/5ef9aa780dc974779708d1eb92644dc73c976c81))


### Features

* new feature ([99ecbe7](https://gitlab.com/aaugmentum/pipelines-playground/commit/99ecbe74d7b3eca6b90ffd5e71c3e959b45d7776))
* update .gitlab-ci.yml file ([6ce0e2e](https://gitlab.com/aaugmentum/pipelines-playground/commit/6ce0e2e7f15996dc1392eff8e2e3747e27581e24))
* update .releaserc.json ([8b8b1cc](https://gitlab.com/aaugmentum/pipelines-playground/commit/8b8b1ccefcb80bcf556d92fbf12d9994b9b5d849))

# [1.7.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.6.0...v1.7.0) (2023-06-08)


### Features

* hahaha ([abc37d4](https://gitlab.com/aaugmentum/pipelines-playground/commit/abc37d4b4562afc0c92a02fa4f17f77adb2c6865))

# [1.6.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.5.0...v1.6.0) (2023-06-08)


### Features

* update .releaserc.json ([e6fd523](https://gitlab.com/aaugmentum/pipelines-playground/commit/e6fd523d2ed1123c0781a7e561efbfa89cf77aae))

# [1.5.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.4.0...v1.5.0) (2023-06-08)


### Features

* update .gitlab-ci.yml file ([9b4d101](https://gitlab.com/aaugmentum/pipelines-playground/commit/9b4d1016dd01bba4df058d842a65c9ea62d1d4b3))

# [1.4.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.3.0...v1.4.0) (2023-06-08)


### Features

* version artifact ([284e0d3](https://gitlab.com/aaugmentum/pipelines-playground/commit/284e0d31335a108bf70af4cf9363549c264f394c))

# [1.3.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.2.0...v1.3.0) (2023-06-07)


### Features

* add log ([0d9c100](https://gitlab.com/aaugmentum/pipelines-playground/commit/0d9c10007e97671d8c05aebce8256c65e419d23e))

# [1.2.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.1...v1.2.0) (2023-06-07)


### Bug Fixes

* update ver ([89bc931](https://gitlab.com/aaugmentum/pipelines-playground/commit/89bc931fc8fdfab3ecdd6f055635cfb8247185b0))
* update ver ([7a4c5bc](https://gitlab.com/aaugmentum/pipelines-playground/commit/7a4c5bc4e41d7ff5d7f27013b63a6c021b9bfff7))


### Features

* add index.js ([b9fc323](https://gitlab.com/aaugmentum/pipelines-playground/commit/b9fc323a020315acbe0a94fbb0194bf6c5045206))

# [1.2.0-test.1](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.2-test.2...v1.2.0-test.1) (2023-06-07)


### Features

* add index.js ([b9fc323](https://gitlab.com/aaugmentum/pipelines-playground/commit/b9fc323a020315acbe0a94fbb0194bf6c5045206))

## [1.1.2-test.2](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.2-test.1...v1.1.2-test.2) (2023-06-07)


### Bug Fixes

* update ver ([89bc931](https://gitlab.com/aaugmentum/pipelines-playground/commit/89bc931fc8fdfab3ecdd6f055635cfb8247185b0))

## [1.1.2-test.1](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.1...v1.1.2-test.1) (2023-06-07)


### Bug Fixes

* update ver ([7a4c5bc](https://gitlab.com/aaugmentum/pipelines-playground/commit/7a4c5bc4e41d7ff5d7f27013b63a6c021b9bfff7))

## [1.1.1](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.1.0...v1.1.1) (2023-06-07)


### Bug Fixes

* fix lint pipeline ([f4cbbdd](https://gitlab.com/aaugmentum/pipelines-playground/commit/f4cbbddcae9f7756a13c9032952032c0cf7c4d2d))
* fix lint pipeline ([9a30064](https://gitlab.com/aaugmentum/pipelines-playground/commit/9a300648fe5c57dd8f8f02861aa2ede14778e65b))

# [1.1.0](https://gitlab.com/aaugmentum/pipelines-playground/compare/v1.0.0...v1.1.0) (2023-06-07)


### Features

* add prerelease branch ([1a603c0](https://gitlab.com/aaugmentum/pipelines-playground/commit/1a603c0028f08475f382f1bbc9da62e47821a72e))

# 1.0.0 (2023-06-06)


### Continuous Integration

* Update gitlab-ci.yml ([a76d118](https://gitlab.com/aaugmentum/pipelines-playground/commit/a76d1189799d6e81c8d7ea993e5c2c5743aac6cc))


### Features

* version 1.0.0 ([7253b51](https://gitlab.com/aaugmentum/pipelines-playground/commit/7253b515d1ac0a8cd503e7c96d79c39fe94212ec))


### BREAKING CHANGES

*
